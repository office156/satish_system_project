"""
URL configuration for Satish_project project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from myapp.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('convert-to-yaml/', convert_to_yaml_view, name='convert-to-yaml'),
    path('api/admin/get-docker-images', get_docker_images , name="get-docker-images"),
    path('api/admin/get-docker-cdac-images', get_docker_cdac_images , name="get-docker-cdac-images"),
    path('api/admin/get-docker-containers', get_docker_containers , name="get-docker-containers"),
    path('api/admin/get-docker-running-containers', get_docker_running_containers , name="get-docker-running-containers"),
    path('api/admin/get-networks', get_networks , name="get-networks"),
    path('api/admin/get-docker-images-local', get_docker_images_local , name="get-docker-images-local"),
    path('api/admin/pull-docker-images', pull_docker_images , name="pull-docker-images"),
    path('api/admin/load-docker-images-local', load_docker_images_local , name="load-docker-images-local"),
    path('api/admin/get-docker-container-inspect', get_docker_containers_inspect , name="get-docker-container-inspect"),
    path('api/admin/admin-delete-single-image', admin_delete_single_image , name="admin-delete-single-image"),
    path('api/admin/admin-force-delete-single-image', admin_force_delete_single_image , name="admin-force-delete-single-image"),
    path('api/admin/admin-delete-single-container', admin_delete_single_container , name="admin-delete-single-container"),
    path('api/admin/admin-force-delete-single-container', admin_force_delete_single_container , name="admin-force-delete-single-container"),
    path('api/admin/admin-start-container', admin_start_containers , name="admin-start-container"),
    path('api/admin/admin-create-container', admin_create_container , name="admin-create-container"),
    path('api/admin/admin-stop-container', admin_stop_containers , name="admin-stop-container"),
    path('api/admin/admin-kill-container', admin_kill_containers , name="admin-kill-container"),
    path('api/admin/admin-restart-container', admin_restart_containers , name="admin-restart-container"),
    path('api/admin/admin-pause-container', admin_pause_containers , name="admin-pause-container"),
    path('api/admin/admin-resume-container', admin_resume_containers , name="admin-resume-container"),
    path('api/admin/admin-remove-container', admin_remove_containers , name="admin-remove-container"),
    path('api/admin/admin-force-remove-container', admin_force_remove_containers , name="admin-force-remove-container"),
    path('api/admin/get-network-list', admin_get_network_list , name="get-network-list"),

]
