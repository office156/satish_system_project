from django.shortcuts import render
from rest_framework.decorators import api_view
import os
import subprocess
import re 
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse
from django.http import JsonResponse
import json
import yaml
# Create your views here.



@api_view(['GET'])
def get_docker_images(request):
    command = "docker images"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    print(multi_array)
    return Response(multi_array)

@api_view(['GET'])
def get_docker_cdac_images(request):
    command = "docker images | grep -i cdac"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    # print(output)
    lines_output = output.splitlines()
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    print(multi_array)
    return Response(multi_array)

# @api_view(['GET'])
# def get_docker_containers(request):
#     command = "docker ps -a -s"
#     result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#     # Retrieve the output
#     output = result.stdout.strip()
#     array = output.strip().split('\n')
#     line_array=[]
#     for line in array[1:]:
#         single_line = line.split("   ")
#         while("" in single_line):
#             single_line.remove("")
#         line_array.append(single_line)
#     for i in line_array:
#         if(len(i)==7):
#             i.insert(4,"")
#     print(line_array)

#     return Response(line_array)

@api_view(['GET'])
def get_docker_containers(request):
    command = "docker ps -a -s"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    return Response(multi_array)

@api_view(['GET'])
def get_docker_running_containers(request):
    command = "docker ps -s | grep -i cdac"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    print(output)
    lines_output = output.splitlines()
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    return Response(multi_array)

@api_view(['GET'])
def get_networks(request):
    command = "docker network ls"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    print(output)
    lines_output = output.splitlines()
    del lines_output[0]
    multi_array = []

    for string in lines_output:
        substrings = re.split(r'\s+', string)
        multi_array.append(substrings)

    keys = ['networkId', 'name', 'driver' , 'scope']

    result = [{k: v for k, v in zip(keys, sublist)} for sublist in multi_array]

    return Response(result)

@api_view(['GET'])
def get_docker_images_local(request):
    command = "ls ../hpc_tool_img/"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    print(output)
    lines_output = output.splitlines()

    print(lines_output)

    return Response(lines_output)

@api_view(['GET'])
def admin_get_network_list(request):
    command = "docker network ls | awk 'NR>1 {print $2}'"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    print(output)
    lines_output = output.splitlines()

    print(lines_output)

    return Response(lines_output)

@api_view(['POST'])
def load_docker_images_local(request):
    container = request.data['data']['containerId']
    command = "docker load -i ../hpc_tool_img/" + container

    os.system(command)

    return Response("ok")

@api_view(['POST'])
def convert_to_yaml_view(request):
    form_data = request.data  # Assuming the form data is sent as JSON
    try:
        # Use yaml.safe_dump for a human-readable YAML format
        yaml_data = yaml.safe_dump(form_data, default_flow_style=False)

        # Save YAML data to a file (optional)
        with open('output.yaml', 'w') as yaml_file:
            yaml_file.write(yaml_data)

        # You can also include the YAML data in the response
        return Response({'yaml_data': yaml_data}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def pull_docker_images(request):
    image_name = request.data['data']['image']
    command = "docker pull " + image_name

    os.system(command)

    return Response("ok")

@api_view(['POST'])
def get_docker_containers_inspect(request):
    container = request.data['data']['containerId']
    command = "docker container inspect " + container
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # Retrieve the output
    output = result.stdout.strip()
    lines_output = output.splitlines()

    return Response(lines_output)



@api_view(['POST'])
def admin_create_container(request):
    formData = request.data
    name = formData["name"]
    hostip = formData["hostip"]
    hostport = formData["hostport"]
    port = formData["port"]
    network = formData["network"]
    volumetype = formData["volumetype"]
    hostvolume = formData["hostvolume"]
    volume = formData["volume"]
    image = formData["image"]
    user = formData["user"]
    attach = formData["attach"]
    env = formData["env"]
    cmd = formData["cmd"]
    flagcmd = formData["flagcmd"]
    

    print(name)
    print(cmd)
    # print(request.data)

    # command = "mkdef -t node " + hostname +" groups=compute,all" " ip="+ ip +" mac="+ MacAddress+ " netboot=xnba arch=x86_64 mgt=ipmi serialport=0 serialspeed=115200"
    # command = "docker run -itd --name " + name + " --network " + network + " --volume " + volume + " " + image + " " + cmd
    command = "docker run -itd" \
    + (" --name " + name if name else "") \
    + (" --network " + network if network else "") \
    + (" --volume " + volume if volume else "") \
    + (" " + flagcmd if flagcmd else "")
    + (" " + image if image else "") \
    + (" " + cmd if cmd else "")
    # os.system(command)
    print(command)
    try:
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
        if result.returncode == 0:
            # Command executed without errors
            return Response(status=status.HTTP_200_OK)
        else:
            # Command returned a non-zero exit code (indicating an error)
            error_message = f"Error occurred: {result.stderr}"
            return Response(error_message ,status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        error = f"An error occurred: {str(e)}"
        print(error)
        return Response(error ,status=status.HTTP_400_BAD_REQUEST)
    
    # return Response(status=status.HTTP_200_OK)
    # os.system(command);

    # print(formData)
    # print(validate_ip(ip))
    # if(validate_ip(ip)):
    #     os.system(command)
    #     return Response(status=status.HTTP_200_OK)
    # else:
    #     return Response("Incorrect Ip",status=status.HTTP_400_BAD_REQUEST)



@api_view(['DELETE'])
def admin_delete_single_image(request):
        
        image = request.data.get("image")
        command = "docker rmi " + image

        try:
            output = subprocess.check_output(command, shell=True)
        except Exception as e:
            return HttpResponse("Error: Bad Request", status=400)
        
        return Response("ok")
        # output = subprocess.check_output(command, shell=True)
        # print(output.decode("utf-8"))
        # output2 = output.decode("utf-8")
        # test = "error"
        # index = output2.find(test)
        # print(index)

        # if index != -1:
        #     return HttpResponse("Error: Bad Request", status=400)
        # else:
        #     return Response("true")

@api_view(['DELETE'])
def admin_force_delete_single_image(request):
        
        imageId = request.data.get("image")
        image = request.data.get("image2")
        print(image)
        print(imageId)
        # command_stop = "docker stop $(docker ps | grep " + image + ")" + image
        command_stop = "docker stop $(docker ps | grep " + image + " | awk '{print $1}')"
        command = "docker rmi -f " + image
        os.system(command_stop)
        
        if "<none>" in image :
             command = "docker rmi -f " + imageId
             os.system(command)
        else :
             os.system(command)
        
        return Response("ok")

@api_view(['DELETE'])
def admin_delete_single_container(request):
        
        container = request.data.get("container")
        command = "docker rm " + container

        try:
            output = subprocess.check_output(command, shell=True)
        except Exception as e:
            return HttpResponse("Error: Bad Request", status=400)
        
        return Response("ok")

        
@api_view(['DELETE'])
def admin_force_delete_single_container(request):
        
        container = request.data.get("container")
        command_stop = "docker stop " + container
        command = "docker rm -f " + container

        os.system(command_stop)
        os.system(command)
        
        return Response("ok")

@api_view(['POST'])
def admin_start_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker start " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_stop_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker stop " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_kill_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker kill " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_restart_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker restart " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_pause_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker pause " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_resume_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker unpause " + container
            os.system(command)

        return Response("ok")

@api_view(['POST'])
def admin_remove_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker rm " + container
            try:
                output = subprocess.check_output(command, shell=True)
            except Exception as e:
                return HttpResponse("Error: Bad Request", status=400)

        return Response("ok")

@api_view(['POST'])
def admin_force_remove_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            command = "docker rm -f " + container
            try:
                output = subprocess.check_output(command, shell=True)
            except Exception as e:
                return HttpResponse("Error: Bad Request", status=400)

        return Response("ok")

@api_view(['GET'])
def admin_info_containers(request):
        
        containers = request.data['data']['checkedItems']
        for container in containers:
            print(container)
            # command = "docker stop " + container
            # os.system(command)

        return Response("ok")




        
