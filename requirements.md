Landing page
    - Deployment
    - Management
    - User

Deployment page
    - single master node / high availability ?
        - single
        - high (pre-requisite check -> os versions check)
            - Networking -> interface name change -> add / change ipv4 address
            - Passwordless ssh
                - Existing node - > Nodelist (User input) -> Docker network creation 
                    - ip a (Network conditions) (Unimportant)

            - pcs and drbd configuration
                - pcs setup - > drbd setup -> pcs resource creation -> pcs ordering and co-locationing -> pcs&drbd status and pcs constants and pcs config pcs resource crm_mon
            - hpc software stack
                - hpc-tools page (Already created)
                - create env -> form (associated with the image) -> default / custom
            - manage services
                - xcat -> os image -> tools versioning
            - benchmarking