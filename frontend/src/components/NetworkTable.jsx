import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { useEffect } from 'react';
import axios from 'axios';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import globalUrl from '../data/url';


const columns = [
  { field: 'name', headerName: 'Name', width: 130 },
  { field: 'stack', headerName: 'Stack', width: 130 },
  { field: 'driver', headerName: 'Driver', width: 130 },
  { field: 'attachable', headerName: 'Attachable', width: 130 },
  { field: 'ipamdriver', headerName: 'IPAM Driver', width: 130 },
  { field: 'ipv4-ipam-subnet', headerName: 'ipv4 ipam subnet', width: 130 },
  { field: 'ipv4-ipam-gateway', headerName: 'ipv4 ipam gateway', width: 130 },
  { field: 'ipv6-ipam-subnet', headerName: 'ipv6 ipam subnet', width: 130 },
  { field: 'ipv6-ipam-gateway', headerName: 'ipv6 ipam gateway', width: 130 },
  { field: 'ownership', headerName: 'Ownership', width: 130 },
  { field: 'remove', headerName: 'Remove', width: 130 },
//   {
//     field: 'attachable',
//     headerName: 'attachable',
//     type: 'number',
//     width: 90,
//   },
//   {
//     field: 'ipamdriver',
//     headerName: 'Full name',
//     description: 'This column has a value getter and is not sortable.',
//     sortable: false,
//     width: 160,
//     valueGetter: (params) =>
//       `${params.row.firstName || ''} ${params.row.lastName || ''}`,
//   },
];

const rows = [
  { id: 1, name: 'Snow', stack: 'Jon', driver: 35 },
  { id: 2, name: 'Lannister', stack: 'Cersei', driver: 42 },
  { id: 3, name: 'Lannister', stack: 'Jaime', driver: 45 },
  { id: 4, name: 'Stark', stack: 'Arya', driver: 16 },
  { id: 5, name: 'Targaryen', stack: 'Daenerys', driver: null },
  { id: 6, name: 'Melisandre', stack: null, driver: 150 },
  { id: 7, name: 'Clifford', stack: 'Ferrara', driver: 44 },
  { id: 8, name: 'Frances', stack: 'Rossini', driver: 36 },
  { id: 9, name: 'Roxie', stack: 'Harvey', driver: 65 },
];

export default function NetworkTable() {

  const navigate = useNavigate();
  const [data , setdata] = useState([]);

  useEffect(() => {
    fetchData()
  }, []);

  const fetchData = () => {
    axios.get(`${globalUrl}/api/admin/get-networks`).then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })
  };


  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={data}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 5 },
          },
        }}
        pageSizeOptions={[5, 10]}
        checkboxSelection
        getRowId={(row) => row.networkId}
      />
    </div>
  );
}