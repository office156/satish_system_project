import React from "react";

function ScrolledCardImages({ data, ...props }) {
  return (
    <div className={`scrolledCard ${data.color} text-slate-50 flex flex-col justify-between`}>
      <h1 className="pb-3 font-semibold">{data.title}</h1>
      <span className="text-xs px-2 py-1 rounded-full bg-white">
          Load
        </span>

    </div>
  );
}

export default ScrolledCardImages;
