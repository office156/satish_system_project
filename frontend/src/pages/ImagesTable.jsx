import React from "react";
import { Link, useNavigate } from "react-router-dom";
import Datatables from "../components/Datatables/Table";
import TableCell from "../components/Datatables/TableCell";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencil, faRemove, faTrash } from "@fortawesome/free-solid-svg-icons";

function ImagesTable({ loading, dataHeader, data, handleDelete }) {

  const navigate = useNavigate();

  return (
    <Datatables loading={loading} dataHeader={dataHeader}>
      {data?.map((row, index) => (
        <tr
          key={index}
          className="bg-white border md:border-b block md:table-row rounded-md shadow-md md:rounded-none md:shadow-none mb-5"
        >
          <TableCell dataLabel="Name" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[0]}
            </span>
          </TableCell>
          <TableCell dataLabel="Name" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[1]}
            </span>
          </TableCell>
          <TableCell dataLabel="Name" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[2]}
            </span>
          </TableCell>
          <TableCell dataLabel="Name" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[3]} {row[4]} {row[5]}
            </span>
          </TableCell>
          <TableCell dataLabel="Name" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[6]}
            </span>
          </TableCell>
          {/* <TableCell dataLabel="Email" showLabel={true}>
            <p className="font-normal text-sm text-gray-500">{row.email}</p>
          </TableCell>
          <TableCell dataLabel="Email" showLabel={true}>
            <p className="font-normal text-sm text-gray-500">{row.username}</p>
          </TableCell>
          <TableCell dataLabel="Role" showLabel={true}>
            <span className=" space-x-1">
              {row.roles?.map((role, index) => (
                <span
                  key={index}
                  className="rounded-full py-1 px-3 text-xs font-semibold bg-emerald-200 text-green-900"
                >
                  {role.name}
                </span>
              ))}
            </span>
          </TableCell> */}
          <TableCell >
            <Link
              onClick={(e) => {
                e.preventDefault();
                handleDelete(row[2],row[0],row[1]);
              }}
              to={`/auth/master/user/${row.id}/edit`}
              className={`text-red-700 inline-flex py-2 px-2 rounded  text-sm`}
            >
              <FontAwesomeIcon icon={faTrash} />
            </Link>
          </TableCell>

          <TableCell>
          <button className='border ml-4 bg-sky-400 text-white rounded-lg p-2' onClick={()=>{
            sessionStorage.setItem("selectedImage", row[2]);
            navigate("/image/container/create")
          }}>Create container</button>
          </TableCell>
        </tr>
      ))}
    </Datatables>
  );
}

export default ImagesTable;
