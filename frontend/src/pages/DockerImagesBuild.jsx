import React from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload , faCrosshairs, faWrench, faFileLines, faPlus, faTrashCan, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import "./MyCss.css"

export default function DockerImagesBuild() {
    const [sidebarToggle] = useOutletContext();


  return (
    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />

      <div className="m-3 text-2xl">
  Build image
</div>

      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">

            <div className="topButtons mb-2">
              <button className='border px-2 py-2 bg-gray-300 text-gray-600 mr-5'><span className='mr-1'><FontAwesomeIcon icon={faWrench} /></span> Builder</button>
              
              <button className='text-gray-600'><span className='mr-1'><FontAwesomeIcon icon={faFileLines} /></span> Output</button>
            </div>

            <p className='font-semibold text-gray-700 mb-2'>Naming</p>
            <p className='text-sm text-gray-400 mb-3'>You can specify multiple names to your image.</p>

            <div className='mb-3'>
            <span className='text-gray-600 mr-1'>Names</span>
            <button className='px-2 py-none text-sm border bg-gray-500 text-white rounded'><span className='text-xs'><FontAwesomeIcon icon={faPlus} /></span> add additional name</button>
            </div>

            <p className='text-sm text-gray-400'>
            A name must be specified in one of the following formats:  <span className='text-gray-700'>name:tag</span>, <span className='text-gray-700'>repository/name:tag</span>  or <span className='text-gray-700'>registryfqdn:port/repository/name:tag</span> format. If you omit the tag the default <span className='font-semibold text-gray-600'>latest</span> value is assumed.
            </p>


            <div className='my-5 ml-5'>
            <input type="text" className='border rounded px-2' placeholder='e.g. my-image:my-tag'/>
            <button className='border border-red-600 px-1 ml-1 rounded text-red-500 '><FontAwesomeIcon icon={faTrashCan}></FontAwesomeIcon> </button>
           <br />
           <span className='text-sm text-orange-700'><FontAwesomeIcon icon={faExclamationTriangle} />The image name must consist of between 2 and 255 lowercase alphanumeric characters, '.', '_', or '-' (e.g. 'my-name', or 'abc-123').</span>
            </div>

            <div className='mb-5'>
            <p className='font-semibold text-gray-700 mb-2'>Build method</p>
            
<h3 class="mb-5 text-lg font-medium text-gray-900 dark:text-white">Choose technology:</h3>
<ul class="grid w-full gap-6 md:grid-cols-3">
    <li>
        <input type="radio" id="react-option" value="" name='build-method' class="hidden peer" required=""></input>
        <label for="react-option" class="flex items-center justify-between w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">                           
            <div class="block">
                <div class="w-full text-lg font-semibold">Web editor</div>
                <div class="w-full text-sm">Use our Web editor</div>
            </div>
        </label>
    </li>
    <li>
        <input type="radio" id="flowbite-option" value="" name='build-method' class="hidden peer"></input>
        <label for="flowbite-option" class="flex items-center justify-between w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">
            <div class="block">
                <div class="w-full text-lg font-semibold">Upload</div>
                <div class="w-full text-sm">Upload from your computer</div>
            </div>
        </label>
    </li>
    <li>
        <input type="radio" id="angular-option" value="" name='build-method' class="hidden peer"></input>
        <label for="angular-option" class="flex items-center justify-between w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">
            <div class="block">
                <div class="w-full text-lg font-semibold">URL</div>
                <div class="w-full text-sm">Specify a URL to a file</div>
            </div>
        </label>
    </li>
</ul>

            </div>

            <div className='mb-5'>
            <p className='font-semibold text-gray-700 mb-2'>Web editor</p>
            </div>

 {/* <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                <FontAwesomeIcon icon={faDownload} /></span>Build Image</div>

            <div className="">

            <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                    <span className='mr-2 w-20'>Registry</span>
                    <input type="text" placeholder='Docker Hub (anonymous)' className='border flex-1'/>
                </div>
                <div className="line2 flex">
                    <span className='mr-2 w-20'>Image <span className='text-red-700'>*</span></span>
                    <input type="text" placeholder='e.g.  my image:my tag' className='border flex-1'/>
                    <button className='search-button border ml-2'>Search</button>
                </div>
                <div className="line3 mt-3">
                    <a href="/image/add/local" className='text-cyan-600'> <span><FontAwesomeIcon icon={faCrosshairs} /></span> Advanced mode</a>
                </div>
                <div className="line4 mt-3">
                    <button className='py-2 px-4 border border-emerald-500 bg-emerald-600 rounded text-gray-200 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm'>Pull the image</button>
                </div>
            </div>

            </div> */}
          </div>
        </div>

      </main>
      </>
  )
}
