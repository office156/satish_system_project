import React, { useEffect, useState } from 'react'
import Navbar from "../components/Navbar/Index";
import ImagesTable from "./ImagesTable";
import { useNavigate, useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload, faCrosshairs, faListUl, faSearch, faCross, faXmark, faTrashCan, faFileImport, faUpload, faPlus } from '@fortawesome/free-solid-svg-icons';
import "./MyCss.css"
import axios from 'axios';
import dataHeader from '../data/DockerImagesHeader';
import { toast, ToastContainer } from 'react-toastify';
import { faDocker } from '@fortawesome/free-brands-svg-icons';
import globalUrl from '../data/url';

export default function DockerImagesAdd() {
  const [sidebarToggle] = useOutletContext();
  const [data, setdata] = useState([]);
  const [imageName, setimageName] = useState('');

  const [loading] = useState(false);

  const navigate = useNavigate();

  const openExternalLinkInNewTab = (externalLink) => {
    window.open(externalLink, '_blank');
  };


  useEffect(() => {
    fetchData()
  }, []);

  const fetchData = () => {
    axios.get(`${globalUrl}/api/admin/get-docker-images`).then((response) => {
      console.log(response.data);
      console.log(response)
      //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error) => {
      //   console.log(error)
    })
  };

  const pullImage = (image) => {

    document.body.style.cursor = 'wait';
    if (image !== '') {
      axios.post(`${globalUrl}/api/admin/pull-docker-images`, { data: { image } }).then((response) => {
        console.log(response);
        document.body.style.cursor = 'default';
        toast.success("Image pulled successfully")
        setimageName('');

        fetchData()
      }).catch((error) => {
        //   console.log(error)
        document.body.style.cursor = 'default';
        toast.error(error)

      })
    } else {
      // Handle logic for the case when inputValue is empty
      document.body.style.cursor = 'default';
      toast.error('Please enter something before clicking the button.');
    }



  }

  const handleDelete = (image, imageName, imageTag) => {

    axios.delete(`${globalUrl}/api/admin/admin-delete-single-image`, { data: { image } }).then((response) => {
      console.log(response);
      toast.success("Image deleted successfully")
      fetchData()

    }).catch((error) => {
      console.log(error)
      const image2 = imageName + ":" + imageTag
      handleAction(image, image2)
    })
    // axios.delete("")
  }

  const handleForceDelete = (image, image2) => {

    axios.delete(`${globalUrl}/api/admin/admin-force-delete-single-image`, { data: { image, image2 } }).then((response) => {
      console.log(response);
      toast.success("Image deleted successfully")
      fetchData()

    }).catch((error) => {
      console.log(error)
      toast.error("Server error")
    })
    // axios.delete("")
  }

  const handleAction = (image, image2) => {
    const userConfirmed = window.confirm('WARNING!!!!! : The container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      handleForceDelete(image, image2)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }



  return (
    <>
      <main className="h-full">
        <Navbar toggle={sidebarToggle} />


        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
              <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                <FontAwesomeIcon icon={faDownload} /></span>Pull Image</div>

            <div className="">

              <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                  <span className='mr-2 w-20'>Registry</span>
                  <input type="text" placeholder='Docker Hub (anonymous)' className='border flex-1 commonInputStyle' disabled />
                </div>
                <div className="line2 flex">
                  <span className='mr-2 w-20'>Image <span className='text-red-700'>*</span></span>
                  <input type="text" placeholder='e.g.  my image:my tag' className='border flex-1 commonInputStyle' value={imageName} onChange={(e) => {
                    setimageName(e.target.value)
                  }} />
                  <button onClick={() => openExternalLinkInNewTab('https://hub.docker.com/search?type=image&q=')} className='search-button border ml-2 '><FontAwesomeIcon icon={faDocker} /> <span className='ml-1'>Search</span></button>
                </div>
                <div className="line3 mt-3">
                  <a href="/image/add/local" className='text-cyan-600'> <span><FontAwesomeIcon icon={faCrosshairs} /></span> Advanced mode</a>
                </div>
                <div className="line4 mt-3">
                  <button className='py-2 px-4 focus:outline-none text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:ring-purple-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-900 justify-end text-sm' onClick={() => {
                    pullImage(imageName)
                  }} >Pull the image</button>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">

            <div className="mb-5 flex items-center justify-between overflow-x-auto">
              <div className='text-xl text-slate-500 min-w-max mr-2'>
                <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                  <FontAwesomeIcon icon={faListUl} /></span> Images
              </div>

              <div className='flex'>
              <div class="mr-5 relative">
          <input type="text" class="border rounded-md focus:outline-none hover:cursor-pointer py-2 px-2" name="" placeholder='Search....'></input>
          <FontAwesomeIcon className='absolute top-3 right-3 text-gray-500 hover:cursor-pointer' icon={faXmark} />
          <span class="absolute top-4 right-5 border-l pl-4"><i class="fa fa-microphone text-gray-500 hover:text-green-500 hover:cursor-pointer"></i></span>
        </div>

        <button className='flex py-2.5 px-5 me-2 mr-5 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-md border border-gray-400 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700'><span className='mr-1'><FontAwesomeIcon icon={faTrashCan} /></span> Remove</button>
          
          <button className='flex py-2.5 px-5 me-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-md border border-gray-400 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700'><span className='mr-1'><FontAwesomeIcon icon={faDownload} /></span> Import</button>
          
          <button className='flex py-2.5 px-5 me-2 mr-5 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-md border border-gray-400 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700'><span className='mr-1'><FontAwesomeIcon icon={faUpload} /></span> Export</button>
          
          <button className='h-11 min-w-max text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-md text-sm px-5 py-2.5 text-center me-2' onClick={()=>{
            navigate('/image/build')
          }}><FontAwesomeIcon className='mr-1' icon={faPlus} />Build a new image</button>

              </div>

              
            </div>

            

            <ImagesTable
              loading={loading}
              dataHeader={dataHeader}
              data={data}
              handleDelete={handleDelete}
            />
          </div>
        </div>
      </main>
    </>
  )
}
