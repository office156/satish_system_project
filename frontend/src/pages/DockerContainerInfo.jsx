import React from 'react'
import Navbar from "../components/Navbar/Index";
import { useNavigate , useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload , faCrosshairs, faGear, faScaleUnbalancedFlip, faCubes, faQuestion, faFileInvoice, faCircleInfo, faChartSimple, faTerminal, faPaperclip, faQuestionCircle, faPenToSquare, faEye, faList, faDiagramProject } from '@fortawesome/free-solid-svg-icons';
import { useState , useEffect } from 'react';
import axios from 'axios';
import { ToastContainer , toast } from 'react-toastify';
import { faBarsProgress, faBomb, faFloppyDisk, faPause, faPlay, faPlus, faRepeat, faSkull, faStop, faTrashCan } from "@fortawesome/free-solid-svg-icons";

import "./MyCss.css"
import globalUrl from '../data/url';

export default function DockerContainerInfo() {
    const [sidebarToggle] = useOutletContext();

  const [loading] = useState(false);
  const [data , setdata] = useState([]);
  const [checkedItems, setCheckedItems] = useState([]);
  const navigate = useNavigate();

   useEffect(() => {
    // fetchData()
  }, []);

  const fetchData = () => {
    axios.get(`${globalUrl}/api/admin/get-docker-containers`).then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })
  };

  const handleDelete = (container) =>{

    axios.delete(`${globalUrl}/api/admin/admin-delete-single-container`,{data:{container}}).then((response)=>{
      console.log(response);
      toast.success("Container deleted successfully")
      fetchData()
  
    }).catch((error)=>{
      console.log(error)
      handleAction(container)
    })
    // axios.delete("")
  }

  const handleForceDelete = (container) =>{

    axios.delete(`${globalUrl}/api/admin/admin-force-delete-single-container`,{data:{container}}).then((response)=>{
      console.log(response);
      toast.success("Container deleted successfully")
      fetchData()
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error("Server error")
      document.body.style.cursor = 'default';
    })
    // axios.delete("")
  }

  const handleAction = (container) => {
    const userConfirmed = window.confirm('WARNING!!!!! : The container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      document.body.style.cursor = 'wait';
      handleForceDelete(container)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const handleActionRemove = () => {
    const userConfirmed = window.confirm('WARNING!!!!! : One of the container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      document.body.style.cursor = 'wait';
      handleForceRemove()
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const handleCheckboxChange = (event) => {
    const { value, checked } = event.target;

    if (checked) {
      setCheckedItems([...checkedItems, value]);
    } else {
      setCheckedItems(checkedItems.filter(item => item !== value));
    }
  };

  const handleStart = () => {
    axios.post(`${globalUrl}/api/admin/admin-start-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers started successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleStop = () => {
    document.body.style.cursor = 'wait';
    axios.post(`${globalUrl}/api/admin/admin-stop-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers stopped successfully")
      fetchData()
      setCheckedItems([])
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      document.body.style.cursor = 'default';
      // handleAction(container)
    })
  }

  const handleKill = () => {
    axios.post(`${globalUrl}/api/admin/admin-kill-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers killed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleRestart = () => {
    axios.post(`${globalUrl}/api/admin/admin-restart-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers restarted successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handlePause = () => {
    axios.post(`${globalUrl}/api/admin/admin-pause-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers paused successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleResume = () => {
    axios.post(`${globalUrl}/api/admin/admin-resume-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers resumed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleRemove = () => {
    axios.post(`${globalUrl}/api/admin/admin-remove-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers removed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      handleActionRemove()
    })
  }

  const handleForceRemove = () => {
    axios.post(`${globalUrl}/api/admin/admin-force-remove-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers removed successfully")
      fetchData()
      setCheckedItems([])
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      document.body.style.cursor = 'default';
      // handleAction(container)
    })
  }



  return (
    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />


      <div className="navigation-tabs ml-2">
        <a href="/container" className='anchor_tag text-md'>Container</a> <span> {'>'} </span> <a href="" className='anchor_tag'>name</a>
      </div>

      <div className='ml-3 text-3xl mt-2'>
        Container details
      </div>

      <div className="mainCard">
      <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
      <div className='actions-header text-xl text-slate-500 mb-5'>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faGear} /></span>Actions</div>
        <div className="flex ">
          <button className="border py-2 px-3 text-white bg-green-500" onClick={()=>{ handleStart() }}><FontAwesomeIcon icon={faPlay} />Start</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleStop() }}><FontAwesomeIcon icon={faStop} />Stop</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleKill() }}><FontAwesomeIcon icon={faBomb} />Kill</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handleRestart() }}><FontAwesomeIcon icon={faRepeat} />Restart</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handlePause() }}><FontAwesomeIcon icon={faPause} />Pause</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handleResume() }}><FontAwesomeIcon icon={faPlay} />Resume</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleRemove() }}><FontAwesomeIcon icon={faTrashCan} />Remove</button>
        </div>
        </div>
        </div>


      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white rounded-md">
            <div className='text-xl text-slate-500 py-4 px-6 '>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faCubes} /></span>Container status</div>

            <div className="">
              
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>ID</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>Name</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>Status</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>Created</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>Start time</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 flex items-center'>
                <p className='text-md text-slate-500 pl-6'>Container webhook <FontAwesomeIcon className='' icon={faQuestionCircle} /></p>
              </div>
              <div className='border-t px-4 py-2 flex items-center text-blue-400 pb-5'>
                <a href="" className="anchor_tag ml-2"><FontAwesomeIcon icon={faFileInvoice} /> Logs</a>
                <a href="/container/inspect" className="anchor_tag ml-3"><FontAwesomeIcon icon={faCircleInfo} /> Inspect</a>
                <a href="" className="anchor_tag ml-3"><FontAwesomeIcon icon={faChartSimple} /> Stats</a>
                <a href="/container/inspect/console" className="anchor_tag ml-3"><FontAwesomeIcon icon={faTerminal} /> Console</a>
                <a href="" className="anchor_tag ml-3"><FontAwesomeIcon icon={faPaperclip} /> Attach</a>
              </div>

            </div>
          </div>
        </div>


      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white rounded-md">
            <div className='text-xl text-slate-500 py-4 px-6 '>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faEye} /></span>Access control</div>

            <div className="">
              
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>Ownership</p>
                <p>administrators <FontAwesomeIcon className='' icon={faQuestionCircle} /></p>
              </div>
              <div className='px-4 py-2 flex items-center text-blue-400 pb-5'>
                <a href="" className="anchor_tag ml-2"><FontAwesomeIcon icon={faPenToSquare} /> Change ownership</a>
              </div>

            </div>
          </div>
        </div>

        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faList} /></span>Create Image</div>

                <br />
                <p className="font-medium text-amber-700">You can create an image from this container , this allows you to backup important data or save helpful configurations. You'll be able to spin up another container based on this image afterwards.</p>
            <div className="">

            <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                    <span className='mr-2 w-20'>Repositary</span>
                    <input  type="text" placeholder='Local repositary' className='border flex-1 commonInputStyle'/>
                </div>
                <div className="line2 flex">
                    <span className='mr-2 w-20'>Image <span className='text-red-700'>*</span></span>
                    <input  type="text" placeholder='e.g.  my image:my tag' className='border flex-1 commonInputStyle'/>
                    <button className='search-button border ml-2'>Search</button>
                </div>
                <div className="line4 mt-3">
                    <button className='py-2 px-4 border text-green-900 bg-green-200 rounded text-gray-200 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm'>Create</button>
                </div>
            </div>

            </div>
          </div>
        </div>



        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white rounded-md">
            <div className='text-xl text-slate-500 py-4 px-6 '>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faList} /></span>Container details</div>

            <div className="">
              
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>IMAGE</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>CMD</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>ENTRYPOINT</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>ENV</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>LABELS</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 grid grid-cols-4 gap-4'>
                <p className='text-md text-slate-500 pl-6'>RESTART POLICIES</p>
                <p>Testing</p>
              </div>
              <div className='border-t py-2 flex items-center'>
                <p className='text-md text-slate-500 pl-6'>Container webhook <FontAwesomeIcon className='' icon={faQuestionCircle} /></p>
              </div>
              <div className='border-t px-4 py-2 flex items-center text-blue-400 pb-5'>
                <a href="" className="anchor_tag ml-2"><FontAwesomeIcon icon={faFileInvoice} /> Logs</a>
                <a className="anchor_tag ml-3"><FontAwesomeIcon icon={faCircleInfo} /> Inspect</a>
                <a href="" className="anchor_tag ml-3"><FontAwesomeIcon icon={faChartSimple} /> Stats</a>
                <a href="" className="anchor_tag ml-3"><FontAwesomeIcon icon={faTerminal} /> Console</a>
                <a href="" className="anchor_tag ml-3"><FontAwesomeIcon icon={faPaperclip} /> Attach</a>
              </div>

            </div>
          </div>
        </div>


        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md text-green-900 bg-green-200'>
                <FontAwesomeIcon icon={faDiagramProject} /></span>Connected networks</div>

                <br />
            <div className="">

            <div className="form mt-5">
                <div className="line2 flex">
                    <span className='mr-2 w-20'>Join a network <span className='text-red-700'>*</span></span>
                    <input type="text" placeholder='e.g.  my image:my tag' className='border flex-1 commonInputStyle'/>
                    <button className='search-button border ml-2'>Join network</button>
                </div>
            </div>

            </div>
          </div>
        </div>

      </main>
      </>
  )
}
