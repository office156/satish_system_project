import React, { useEffect, useState } from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload , faCrosshairs, faCirclePlus , faStarOfLife } from '@fortawesome/free-solid-svg-icons';
import "./MyCss.css"
import axios from 'axios';
import { toast , ToastContainer } from 'react-toastify';
import globalUrl from "../data/url.js";

export default function DockerImageCreateContainer() {
    const [sidebarToggle] = useOutletContext();
    const [advanced , setadvanced] = useState(false)
    const [Addoptions , setAddoptions] = useState(false);
    const [data , setdata] = useState([]);

    const text = `Usage:  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
Create and run a new container from an image
Aliases:
  docker container run, docker run
Options:
      --add-host list                    Add a custom host-to-IP mapping (host:ip)
      --annotation map                   Add an annotation to the container (passed through to the OCI runtime) (default map[])
  -a, --attach list                      Attach to STDIN, STDOUT or STDERR
      --blkio-weight uint16              Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)
      --blkio-weight-device list         Block IO weight (relative device weight) (default [])
      --cap-add list                     Add Linux capabilities
      --cap-drop list                    Drop Linux capabilities
      --cgroup-parent string             Optional parent cgroup for the container
      --cgroupns string                  Cgroup namespace to use (host|private)
                                         'host':    Run the container in the Docker host's cgroup namespace
                                         'private': Run the container in its own private cgroup namespace
                                         '':        Use the cgroup namespace as configured by the
                                                    default-cgroupns-mode option on the daemon (default)
      --cidfile string                   Write the container ID to the file
      --cpu-period int                   Limit CPU CFS (Completely Fair Scheduler) period
      --cpu-quota int                    Limit CPU CFS (Completely Fair Scheduler) quota
      --cpu-rt-period int                Limit CPU real-time period in microseconds
      --cpu-rt-runtime int               Limit CPU real-time runtime in microseconds
  -c, --cpu-shares int                   CPU shares (relative weight)
      --cpus decimal                     Number of CPUs
      --cpuset-cpus string               CPUs in which to allow execution (0-3, 0,1)
      --cpuset-mems string               MEMs in which to allow execution (0-3, 0,1)
  -d, --detach                           Run container in background and print container ID
      --detach-keys string               Override the key sequence for detaching a container
      --device list                      Add a host device to the container
      --device-cgroup-rule list          Add a rule to the cgroup allowed devices list
      --device-read-bps list             Limit read rate (bytes per second) from a device (default [])
      --device-read-iops list            Limit read rate (IO per second) from a device (default [])
      --device-write-bps list            Limit write rate (bytes per second) to a device (default [])
      --device-write-iops list           Limit write rate (IO per second) to a device (default [])
      --disable-content-trust            Skip image verification (default true)
      --dns list                         Set custom DNS servers
      --dns-option list                  Set DNS options
      --dns-search list                  Set custom DNS search domains
      --domainname string                Container NIS domain name
      --entrypoint string                Overwrite the default ENTRYPOINT of the image
  -e, --env list                         Set environment variables
      --env-file list                    Read in a file of environment variables
      --expose list                      Expose a port or a range of ports
      --gpus gpu-request                 GPU devices to add to the container ('all' to pass all GPUs)
      --group-add list                   Add additional groups to join
      --health-cmd string                Command to run to check health
      --health-interval duration         Time between running the check (ms|s|m|h) (default 0s)
      --health-retries int               Consecutive failures needed to report unhealthy
      --health-start-interval duration   Time between running the check during the start period (ms|s|m|h) (default 0s)
      --health-start-period duration     Start period for the container to initialize before starting health-retries
                                         countdown (ms|s|m|h) (default 0s)
      --health-timeout duration          Maximum time to allow one check to run (ms|s|m|h) (default 0s)
      --help                             Print usage
  -h, --hostname string                  Container host name
      --init                             Run an init inside the container that forwards signals and reaps processes
  -i, --interactive                      Keep STDIN open even if not attached
      --ip string                        IPv4 address (e.g., 172.30.100.104)
      --ip6 string                       IPv6 address (e.g., 2001:db8::33)
      --ipc string                       IPC mode to use
      --isolation string                 Container isolation technology
      --kernel-memory bytes              Kernel memory limit
  -l, --label list                       Set meta data on a container
      --label-file list                  Read in a line delimited file of labels
      --link list                        Add link to another container
      --link-local-ip list               Container IPv4/IPv6 link-local addresses
      --log-driver string                Logging driver for the container
      --log-opt list                     Log driver options
      --mac-address string               Container MAC address (e.g., 92:d0:c6:0a:29:33)
  -m, --memory bytes                     Memory limit
      --memory-reservation bytes         Memory soft limit
      --memory-swap bytes                Swap limit equal to memory plus swap: '-1' to enable unlimited swap
      --memory-swappiness int            Tune container memory swappiness (0 to 100) (default -1)
      --mount mount                      Attach a filesystem mount to the container
      --name string                      Assign a name to the container
      --network network                  Connect a container to a network
      --network-alias list               Add network-scoped alias for the container
      --no-healthcheck                   Disable any container-specified HEALTHCHECK
      --oom-kill-disable                 Disable OOM Killer
      --oom-score-adj int                Tune host's OOM preferences (-1000 to 1000)
      --pid string                       PID namespace to use
      --pids-limit int                   Tune container pids limit (set -1 for unlimited)
      --platform string                  Set platform if server is multi-platform capable
      --privileged                       Give extended privileges to this container
  -p, --publish list                     Publish a container's port(s) to the host
  -P, --publish-all                      Publish all exposed ports to random ports
      --pull string                      Pull image before running ("always", "missing", "never") (default "missing")
  -q, --quiet                            Suppress the pull output
      --read-only                        Mount the container's root filesystem as read only
      --restart string                   Restart policy to apply when a container exits (default "no")
      --rm                               Automatically remove the container and its associated anonymous volumes when it exits
      --runtime string                   Runtime to use for this container
      --security-opt list                Security Options
      --shm-size bytes                   Size of /dev/shm
      --sig-proxy                        Proxy received signals to the process (default true)
      --stop-signal string               Signal to stop the container
      --stop-timeout int                 Timeout (in seconds) to stop a container
      --storage-opt list                 Storage driver options for the container
      --sysctl map                       Sysctl options (default map[])
      --tmpfs list                       Mount a tmpfs directory
  -t, --tty                              Allocate a pseudo-TTY
      --ulimit ulimit                    Ulimit options (default [])
  -u, --user string                      Username or UID (format: <name|uid>[:<group|gid>])
      --userns string                    User namespace to use
      --uts string                       UTS namespace to use
  -v, --volume list                      Bind mount a volume
      --volume-driver string             Optional volume driver for the container
      --volumes-from list                Mount volumes from the specified container(s)
  -w, --workdir string                   Working directory inside the container`

    const [formData, setFormData] = useState({
        name: '',
        hostip: '',
        hostport: '',
        port: '',
        network: '',
        volumetype: '',
        hostvolume: '',
        volume: '',
        image: '',
        user: '',
        attach: '',
        env: '',
        cmd: '',
        flagcmd:''
      });

      const [selectedImage, setSelectedImage] = useState("");

      useEffect(() => {
        fetchData()
        const savedValue = sessionStorage.getItem("selectedImage");
        if (savedValue) {
          setFormData((prevFormData) => ({
            ...prevFormData, // Spread the existing fields
            image: savedValue, // Update only the `image` field
          }));
          console.log("Restored value from sessionStorage:", savedValue);
        }
      }, []);
    
      const fetchData = () => {
        axios.get(`${globalUrl}/api/admin/get-networks`).then((response)=>{
          console.log(response.data);
          console.log(response)
        //   console.log(response.data.image_list[0]);
          setdata(response.data)
        }).catch((error)=>{
        //   console.log(error)
        })
      };

      const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
      };

      const handleAdvancedChange = () => {
        setadvanced(!advanced)
      };

      const handleAddoptionsChange = () => {
        setAddoptions(!Addoptions)
      };


      const clearPage = () => {
        setFormData({
            name: '',
            hostip: '',
            hostport: '',
            port: '',
            network: '',
            volumetype: '',
            hostvolume: '',
            volume: '',
            image: '',
            user: '',
            attach: '',
            env: '',
            cmd: '',
            flagcmd: ''
        });
      };
    
      const handleSubmit = () => {
        // event.preventDefault();
        console.log(formData);
        axios.post(`${globalUrl}/api/admin/admin-create-container`,formData).then((response)=>{
            console.log(response);
            toast.success("Container created successfully")
            clearPage()
          }).catch((error)=>{
            console.log(error)
            toast.error(error.response.data)
            // handleAction(container)
          })

      };

  return (
    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />


      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                <FontAwesomeIcon icon={faCirclePlus} /></span>Create container</div>

            <div className="">

            <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Name <span class="star">*</span></span>
                    <input type="text" placeholder='Assign a name to the container' className='border flex-1 commonInputStyle' name="name" value={formData.name} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full mt-4">
                    <span className='mr-4 w-20'>Network <span class="star">*</span></span>
                    {/* <input type="text" placeholder='Connect a container to a network' className='border flex-1 commonInputStyle' name="network" value={formData.network} onChange={handleInputChange}/> */}
                    {/* <select class="form-select" aria-label="Default select example">
  <option selected>Network</option>
  <option value="1">Bridge</option>
  <option value="2">Overlay</option>
  <option value="macvlan">macvlan</option>
  <option value="host">host</option>
  <option value="ipvlan">ipvlan</option>
  <option value="None">None</option>
</select> */}
    <select className="form-select" aria-label="Default select example" name="network" value={formData.network} onChange={handleInputChange}>
      <option value="" disabled>Network</option>
      {data.map((item, index) => (
        <option key={index}  value={item.name}>
          {item.name}
        </option>
      ))}
    </select>
                </div>
                <div class="flex items-center mb-5">
    <input id="checked-checkbox" type="checkbox" name="advanced" value={advanced} onChange={handleAdvancedChange} class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"></input>
    <label for="checked-checkbox" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Advanced</label>
</div>


{/* Advanced section */}

{advanced && <>
    <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>HostIP</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="hostip" value={formData.hostip} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>HostPort</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="hostport" value={formData.hostport} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Port</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="port" value={formData.port} onChange={handleInputChange}/>
                </div>
</>}

{/*  */}


                {/* <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>VolumeType</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="volumetype" value={formData.volumetype} onChange={handleInputChange}/>
                </div> */}
                {/* <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>HostVolume</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="hostvolume" value={formData.hostvolume} onChange={handleInputChange}/>
                </div> */}
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Volume</span>
                    <input type="text" placeholder='Bind mount a volume (format: /path/to/dir:/inside/container/path/to/dir)' className='border flex-1 commonInputStyle' name="volume" value={formData.volume} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>User</span>
                    <input type="text" placeholder='Username or UID (format: <name|uid>[:<group|gid>])' className='border flex-1 commonInputStyle' name="user" value={formData.user} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Attach</span>
                    <input type="text" placeholder='Attach to STDIN, STDOUT or STDERR' className='border flex-1 commonInputStyle' name="attach" value={formData.attach} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Env</span>
                    <input type="text" placeholder='Set environment variables' className='border flex-1 commonInputStyle' name="env" value={formData.env} onChange={handleInputChange}/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Extra options</span>
                    <input type="text" placeholder='Additional options' className='border flex-1 commonInputStyle' name="flagcmd" value={formData.flagcmd} onChange={handleInputChange}/>
                </div>

                    <div class="flex items-center mb-5">
                    <input id="checked-checkbox" type="checkbox" name="Addoptions" value={Addoptions} onChange={handleAddoptionsChange} class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"></input>
    <label for="checked-checkbox" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">docker run --help</label>
</div>

                

                {Addoptions && <div className='card p-2'><pre className='my-2' dangerouslySetInnerHTML={{ __html: text }} /></div>}
                

                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Image</span>
                    <input type="text" className='border flex-1 commonInputStyle' name="image" value={formData.image} disabled/>
                </div>
                <div className="line1 flex w-full my-4">
                    <span className='mr-4 w-20'>Cmd</span>
                    <input type="text" placeholder='Commands to execute after container creation' className='border flex-1 commonInputStyle' name="cmd" value={formData.cmd} onChange={handleInputChange}/>
                </div>
                <div className="line4 mt-3">
                    <button className='py-2 px-4 border border-emerald-500 bg-emerald-600 rounded text-gray-200 hover:bg-white hover:text-black hover:border-emerald-600 justify-end text-sm' onClick={()=>{
                        handleSubmit()
                    }}>Create</button>
                    <button className='ml-3 py-2 px-4 border border-emerald-500 rounded text-black-400 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm' onClick={()=>{
                        clearPage()
                    }}>Cancel</button>
                </div>
            </div>

            </div>
          </div>
        </div>

      </main>
      </>
  )
}
