import React from 'react'
import './Form.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleQuestion, faEyeSlash, faPlus } from '@fortawesome/free-solid-svg-icons'

export default function Form() {
  return (
    <div>
      <p className='ml-2'> <span className='text-primary'>Networks</span> {`>`} Add network</p>

      <h2 className='text-xl font-medium ml-3 mt-4'>Create network</h2>

      <div className="m-5 p-5 border bg-white">
      <form>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label text-secondary">Name</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder='e.g.myNetwork'/>
    {/* <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> */}
  </div>
  <h5 className='my-3 font-medium'>Driver configuration</h5>

<div class="mb-3">
            <label for="disabledSelect" class="form-label text-secondary">Driver</label>
            <select id="disabledSelect" class="form-select">
              <option>bridge</option>
              <option>ipvlan</option>
              <option>macvalan</option>
              <option>overlay</option>
            </select>
          </div>

    <div className="mb-3">
    <label for="exampleInputEmail1" class="form-label text-secondary">Driver options <FontAwesomeIcon icon={faCircleQuestion} /></label>
    <br />
    <button className='btn text-secondary'><FontAwesomeIcon icon={faPlus} /> Add driver option</button>
    </div>

    <div className="mb-3">
      <h5 className='font-medium'>IPV4 Network configuration</h5>
    </div>

    <div className="mb-3">
      <label htmlFor="" className='text-secondary'>Subnet</label>
      <input type="text" className='form-control' placeholder='e.g. 172.20.0.0/16'/>

      <label htmlFor="" className='text-secondary'>Gateway</label>
      <input type="text" className='form-control' placeholder='e.g. 172.20.10.11'/>

      <label htmlFor="" className='text-secondary'>IP range</label>
      <input type="text" className='form-control' placeholder='e.g. 2001:db8::64'/>

      <br />
      <button className='btn text-secondary'><FontAwesomeIcon icon={faPlus} /> Add excluded IP</button>
    </div>

    <div className="mb-3">
      <h5 className='font-medium'>IPV6 Network configuration</h5>
    </div>

    <div className="mb-3">
      <label htmlFor="" className='text-secondary'>Subnet</label>
      <input type="text" className='form-control' placeholder='e.g. 2001:db8::/48'/>

      <label htmlFor="" className='text-secondary'>Gateway</label>
      <input type="text" className='form-control' placeholder='e.g. 2001:db8::1'/>

      <label htmlFor="" className='text-secondary'>IP range</label>
      <input type="text" className='form-control' placeholder='e.g. 2001:db8::/64'/>

      <br />
      <button className='btn text-secondary'><FontAwesomeIcon icon={faPlus} /> Add excluded IP</button>
    </div>

    <div className="mb-3">
      <h5 className='font-medium'>Advanced configuration</h5>
      <button className='btn text-secondary'><FontAwesomeIcon icon={faPlus} /> Add label</button>

    </div>

    <div className="mb-3">
    <div class="mb-3 form-switch">
    <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    <label class="form-check-label" for="exampleCheck1" className='ms-2'>Isolated network <i class="fa-regular fa-circle-question"></i></label>
  </div>
    </div>

    <div className="mb-3">
    <div class="mb-3 form-switch">
    <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    <label class="form-check-label" for="exampleCheck1" className='ms-2'>Enable manual container attachment</label>
  </div>
    </div>

{/* Private card  */}
    <div class="mb-3 border-1 border-sky-500 p-3 d-flex justify-content-between rounded bg-sky-50">

      <div className='d-flex'>
      <FontAwesomeIcon className='me-3 EyeIcon p-3 rounded-full bg-sky-100 text-sky-600' icon={faEyeSlash} />

<div className="card-text">
<p className="font-medium text-lg">Private</p>
<p>I want to restrict this resource to be manageable by myself only</p>
</div>
      </div>

  <div class="checkbox-wrapper-12">
  <div class="cbx">
    <input id="cbx-12" type="checkbox"/>
    <label for="cbx-12"></label>
    <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
      <path d="M2 8.36364L6.23077 12L13 2"></path>
    </svg>
  </div>
  {/* <!-- Gooey--> */}
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
    <defs>
      <filter id="goo-12">
        <fegaussianblur in="SourceGraphic" stddeviation="4" result="blur"></fegaussianblur>
        <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 22 -7" result="goo-12"></fecolormatrix>
        <feblend in="SourceGraphic" in2="goo-12"></feblend>
      </filter>
    </defs>
  </svg>
</div>
</div>


    <div className="mb-3">
      <h5 className='font-medium'>Access control</h5>
    </div>

    <div className="mb-3">
    <div class="mb-3 form-switch">
    <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    <label class="form-check-label" for="exampleCheck1" className='ms-2'>Enable access control <i class="fa-regular fa-circle-question"></i></label>
  </div>
    </div>


    <div className="mb-3">
      <h5>Actions</h5>
    </div>

    <button className='btn btn-primary' disabled>Create the network</button>

  {/* <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" />
  </div>
  <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button> */}
</form>



        {/* <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">@</span>
        <input type="text" class="form-control" placeholder="Username" />
      </div> */}
        
      </div>
    </div>
  )
}
