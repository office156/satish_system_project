import React from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";

export default function DockerImagesSave() {
    const [sidebarToggle] = useOutletContext();


  return (
    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />


      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            {/* <ImagesTable
              loading={loading}
              dataHeader={dataHeader}
              data={data}
              handleDelete={handleDelete}
            /> */}
            Docker Image Save
          </div>
        </div>

      </main>
      </>
  )
}
