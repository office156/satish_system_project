import React from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Datatables from "../components/Datatables/Table";
import TableCell from "../components/Datatables/TableCell";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleInfo, faPencil, faRemove, faTrash } from "@fortawesome/free-solid-svg-icons";

function ContainersTable({ loading, dataHeader, data, handleDelete , checkedItems , handleCheckboxChange}) {

  const navigate = useNavigate();
  
  return (
    <Datatables loading={loading} dataHeader={dataHeader}>
      {data?.map((row, index) => (
        <tr
          key={index}
          className="bg-white border md:border-b block md:table-row rounded-md shadow-md md:rounded-none md:shadow-none mb-5"
        >
          <TableCell dataLabel="" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
            <input
          type="checkbox"
          value={row[0]}
          onChange={handleCheckboxChange}
          checked={checkedItems.includes(row[0])}
        />
            </span>
          </TableCell>
          <TableCell dataLabel="Name" showLabel={true}>
            <a  onClick={() => {
                sessionStorage.setItem("container-id" , row[0])
                navigate("/container/info")
              }} className="anchor_tag font-medium text-sm text-blue-900 ">
            {row[row.length-4]}
            </a>
          </TableCell>
          <TableCell dataLabel="Container Id" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[0]}
            </span>
          </TableCell>
          <TableCell dataLabel="Image" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[1]}
            </span>
          </TableCell>
          <TableCell dataLabel="Command" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[2]}
            </span>
          </TableCell>
          <TableCell dataLabel="Created" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[3]} {row[4]} {row[5]}
            </span>
          </TableCell>
          <TableCell dataLabel="Status" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {/* {row[6]} {row[7]} {row[8]} */}
              {row[6].includes('Up') ? <span className="bg-green-400 p-1 rounded">{row.includes('(Paused)') ? <>Paused</> : <>Running</>}</span> : <span className="bg-red-400 p-1 rounded">Exited</span>}
              {/* {row[6].includes('Up') && row[6].includes('Paused') ? <span className="bg-green-400 p-1 rounded">{row[6]} {row[7]} {row[8]}</span> : <span className="bg-red-400 p-1 rounded">{row[6]} {row[7]} {row[8]} {row[9]} {row[10]}</span>} */}
              {/* {row[6].includes('Up') ? <>true</> : <>false</>} */}
            </span>
          </TableCell>
          <TableCell dataLabel="Ports" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
            {row[row.length-5].includes('/') ? <>{row[row.length-5]}</> : <></>}
            {/* {row[6].includes('Up') && row[row.length-5] != "(Paused)" && row[9].includes('Paused') ? <>{row[row.length-5]}</> : <>{row[6].includes('Exited') ? <></> : <>testing</>}</>}  */}
            </span>
          </TableCell>
          <TableCell dataLabel="Size" showLabel={true}>
            <span className="font-medium text-sm text-gray-900">
              {row[row.length-3]} {row[row.length-2]} {row[row.length-1]}
            </span>
          </TableCell>
          {/* <TableCell dataLabel="Email" showLabel={true}>
            <p className="font-normal text-sm text-gray-500">{row.email}</p>
          </TableCell>
          <TableCell dataLabel="Email" showLabel={true}>
            <p className="font-normal text-sm text-gray-500">{row.username}</p>
          </TableCell>
          <TableCell dataLabel="Role" showLabel={true}>
            <span className=" space-x-1">
              {row.roles?.map((role, index) => (
                <span
                  key={index}
                  className="rounded-full py-1 px-3 text-xs font-semibold bg-emerald-200 text-green-900"
                >
                  {role.name}
                </span>
              ))}
            </span>
          </TableCell> */}
          <TableCell >
            <Link
              onClick={(e) => {
                e.preventDefault();
                handleDelete(row[0]);
              }}
              to={`/auth/master/user/${row.id}/edit`}
              className={`text-red-700 inline-flex py-2 px-2 rounded  text-sm`}
            >
              <FontAwesomeIcon icon={faTrash} />
            </Link>
            <Link
              onClick={(e) => {
                e.preventDefault();
                sessionStorage.setItem("container-id" , row[0])
                navigate("/container/inspect")
              }}
              to={`/auth/master/user/${row.id}/edit`}
              className={`text-blue-700 inline-flex py-2 px-2 rounded  text-sm`}
            >
              <FontAwesomeIcon icon={faCircleInfo} />
            </Link>
          </TableCell>
        </tr>
      ))}
    </Datatables>
  );
}

export default ContainersTable;
