import React, { useState } from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import { useEffect } from 'react';
import axios from 'axios';
import { ToastContainer , toast } from 'react-toastify';
import "./MyCss.css"
import globalUrl from '../data/url';

export default function DockerContainerInspect() {

    const [sidebarToggle] = useOutletContext();
    const [data , setdata] = useState([])


    const containerId = sessionStorage.getItem("container-id")

    useEffect(() => {
        getdata()
      }, []);
    
      const getdata = () => {
        axios.post(`${globalUrl}/api/admin/get-docker-container-inspect`,{data:{containerId}}).then((response)=>{
          // console.log(response.data);
          setdata(response.data)
        }).catch((error)=>{
          console.log(error)
          toast.error(error.message)
          // handleAction(container)
        })
      }


  return (
    <>
          <main className="h-full">
        <Navbar toggle={sidebarToggle} />

        
      <div className="mainCard">

        <div className="simpleHeading mb-5 text-xl text-gray-500 underline">
          Docker Inspect
        </div>


      <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
      <div className="DockerInspectPre">
    <div className="ms-5 mt-5 p-4">
          <pre>
            {data.map((line, lineIndex) => (
              <div className='testing2' key={lineIndex}>{line}</div>
            ))}
            </pre>
      </div>
    </div>
      </div>

    </div>
    </main>

      </>
  )
}
