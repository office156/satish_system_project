import React from 'react'
import { useNavigate } from 'react-router-dom'
// import "./LandingPage.css"

function LandingPage1() {
    const navigate = useNavigate()
  return (
    <div className='landingPageTop container'>
      <h1 className='h1 my-5'>Landing page</h1>

      <p className='my-5'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima enim magnam in commodi quos consequuntur obcaecati eos nulla explicabo, dolorum non libero esse quae reiciendis sit mollitia ad eligendi tempora.
      </p>

      <div className="row">
        <div className="col">
        <div class="card" style={{width: "18rem"}}>
  <img style={{height: "11rem"}} src="https://media.istockphoto.com/id/1400216415/vector/hardware-requirements-blue-gradient-concept-icon.jpg?s=612x612&w=0&k=20&c=-X0D8UEHckIlUZEzNCYxq7vnrl-h4XsS72RVJGUUqDY=" class="card-img-top" alt="..." />
  <div class="card-body">
    <h5 class="h5 card-title">Deployment</h5>
    <p class="card-text my-2">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="/deploy/networking" class="btn btn-primary">Select</a>
  </div>
</div>
        </div>
        <div className="col">
        <div class="card" style={{width: "18rem"}}>
  <img style={{height: "11rem"}} src="https://media.istockphoto.com/id/1490859962/photo/power-soft-skills-multi-skills-responsibility-hr-human-resources-concept-personal-attribute.jpg?s=612x612&w=0&k=20&c=ToS6Yl5eZgQUc0t9FWp7tHkabQvoEe0ge1PFbPYvf7A=" class="card-img-top" alt="..." />
  <div class="card-body">
    <h5 class="card-title h5">Management</h5>
    <p class="card-text my-2">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="/dashboard" class="btn btn-primary">Select</a>
  </div>
</div>
        </div>
        <div className="col">
        <div class="card" style={{width: "18rem"}}>
  <img style={{height: "11rem"}} src="https://media.istockphoto.com/id/1350717417/photo/young-woman-talking-on-smart-phone-at-home-office.jpg?s=612x612&w=0&k=20&c=JyAyPBG9yLIvIiYCjDLEgpY2tVQ76AKNjVOwigXRGV0=" class="card-img-top" alt="..." />
  <div class="card-body">
    <h5 class="card-title h5">User</h5>
    <p class="card-text my-2">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Select</a>
  </div>
</div>
        </div>
      </div>
    </div>

  )
}

export default LandingPage1