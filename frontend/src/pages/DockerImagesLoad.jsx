import React, { useState } from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload , faCrosshairs } from '@fortawesome/free-solid-svg-icons';
import "./MyCss.css"
import axios from 'axios';
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import globalUrl from '../data/url';

export default function DockerImagesLoad() {
    const [sidebarToggle] = useOutletContext();
    const [data , setdata] = useState([]);

    useEffect(() => {
      fetchData()
    }, []);
  
    const fetchData = () => {
      axios.get(`${globalUrl}/api/admin/get-docker-images-local`).then((response)=>{
        console.log(response);

        setdata(response.data)
      }).catch((error)=>{
      //   console.log(error)
      })
    };

    const handleLoad = (containerId) => {
      document.body.style.cursor = 'wait';
      axios.post(`${globalUrl}/api/admin/load-docker-images-local`,{data:{containerId}}).then((response)=>{
        console.log(response);
        document.body.style.cursor = 'default';
        toast.success("Image loaded successfully")
      }).catch((error)=>{
      //   console.log(error)
      document.body.style.cursor = 'default';
      toast.error(error)
      })
    }

    const array2 = ['cardInfo', 'cardWarning', 'cardDanger', 'cardSuccess', 'cardLime' , 'cardDanger']; // Always 5 elements


    const combinedArray = [];
    for (let i = 0; i < data.length; i++) {
      combinedArray.push([data[i], array2[i % 5]]);
    }

  return (


    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />

      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                <FontAwesomeIcon icon={faDownload} /></span>Load Image</div>

            <div className="">

            <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                    <span className='mr-2 w-20'>Repositary</span>
                    <input type="text" placeholder='Local repositary (Absolute path)' className='border flex-1 commonInputStyle'/>
                </div>
                <div className="line2 flex">
                    <span className='mr-2 w-20'>Image <span className='text-red-700'>*</span></span>
                    <input type="text" placeholder='e.g.  my image:my tag' className='border flex-1 commonInputStyle'/>
                    <button className='search-button border ml-2'>Search</button>
                </div>
                <div className="line4 mt-3">
                    <button className='py-2 px-4 border border-emerald-500 bg-emerald-600 rounded text-gray-200 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm'>Load the image</button>
                </div>
            </div>

            </div>
          </div>
        </div>

        {/* <div className="mainCard">
        <div>

        {data.map((item, index) => (
          // Each item in the array generates a list item
          <div className='border w-full border-gray-200 bg-white py-4 px-6 rounded-md flex justify-between items-center mb-2'>
            <div key={index} className="">{item}</div>
            <button className="ImageLocalLoadButton bg-green-500 p-2 rounded">Load</button>
          </div>
          
        ))}

    </div>
</div> */}


<div className='mainCard'>
<div className="grid grid-cols-4 gap-4">

        {combinedArray.map((item, index) => (
    <div className={`p-3 rounded ${item[1]} text-slate-50 flex flex-col overflow-x-hidden hover:overflow-x-scroll no-scrollbar`}>
    <h1 className="pb-3 font-semibold">{item[0]}</h1>
    <span  >
        <button className="text-xs px-2 py-1 rounded-full bg-white" onClick={()=>{
          handleLoad(item[0])}}>Load</button>
      </span>

  </div>
        ))}
        </div>
    </div>


        
      </main>
      </>
  )
}
