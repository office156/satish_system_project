import React, { useState } from "react";
import Navbar from "../components/Navbar/Index";
import { useNavigate, useOutletContext } from "react-router-dom";
import UserTable from "./UserTable";
import ImagesTable from "./ImagesTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBarsProgress, faFloppyDisk, faPlus } from "@fortawesome/free-solid-svg-icons";
// import Images;
import { useEffect } from "react";
import axios from 'axios';
import { toast , ToastContainer } from 'react-toastify';
import NetworkTable from "../components/NetworkTable";
import globalUrl from "../data/url";


function Network() {
  const navigate = useNavigate();
  const [sidebarToggle] = useOutletContext();
  const [data , setdata] = useState([]);

  const [loading] = useState(false);

  const dataHeader = [
    {
      key: "repositary",
      label: "repositary",
    },
    {
      key: "tag",
      label: "tags",
    },
    {
      key: "Image id",
      label: "Image ids",
    },
    {
      key: "Created",
      label: "Created",
    },
    {
      key: "Size",
      label: "Size",
    },
    {
      key: "action",
      label: "actions",
    },
    {
      key: "create",
      label: "create",
    },
  ];

  useEffect(() => {
    // fetchData()
  }, []);

  const fetchData = () => {
    axios.get(`${globalUrl}/api/admin/get-networks`).then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })
  };

  const handleDelete = (image , imageName , imageTag) =>{

    axios.delete(`${globalUrl}/api/admin/admin-delete-single-image`,{data:{image}}).then((response)=>{
      console.log(response);
      toast.success("Image deleted successfully")
      fetchData()
  
    }).catch((error)=>{
      console.log(error)
      const image2 = imageName + ":" + imageTag
      handleAction(image,image2)
    })
    // axios.delete("")
  }

  const handleForceDelete = (image , image2) =>{

    axios.delete(`${globalUrl}/api/admin/admin-force-delete-single-image`,{data:{image , image2}}).then((response)=>{
      console.log(response);
      toast.success("Image deleted successfully")
      fetchData()
  
    }).catch((error)=>{
      console.log(error)
      toast.error("Server error")
    })
    // axios.delete("")
  }

  const handleAction = (image,image2) => {
    const userConfirmed = window.confirm('WARNING!!!!! : The container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      handleForceDelete(image,image2)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const handleImageAdd = () => {
    navigate("/image/add")
  };
  const handleImageLoad = () => {
    navigate("/image/load")
  };
  const handleImageSave = () => {
    navigate("/image/save")
  };

  return (
    <>
      <main className="h-full">
        <Navbar toggle={sidebarToggle} />

<div className="m-3 text-2xl">
  Network
</div>

        {/* Main Content */}
        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <NetworkTable></NetworkTable>
          </div>
        </div>

        <div>

          <button className="border p-2 ml-5 bg-blue-500" onClick={()=>{
            navigate('/add-new-network')
          }}>Add new network</button>
        </div>
        <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop={false}
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      </main>
    </>
  );
}

export default Network;
