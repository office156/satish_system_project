import React, { useState } from "react";
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import UserTable from "./UserTable";
import ImagesTable from "./ImagesTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBarsProgress, faBomb, faFloppyDisk, faPause, faPlay, faPlus, faRepeat, faSkull, faStop, faTrashCan } from "@fortawesome/free-solid-svg-icons";
import ContainersTable from "./ContainersTable";
// import ContainersTable from "./ContainersTable";
import axios from "axios";
import { useEffect } from "react";
import { toast , ToastContainer } from 'react-toastify';
import globalUrl from "../data/url";
// import Images;
function DockerContainers() {
  const [sidebarToggle] = useOutletContext();

  const [loading] = useState(false);
  const [data , setdata] = useState([]);
  const [checkedItems, setCheckedItems] = useState([]);

  const dataHeader = [
    {
      key: "check",
      label: "check",
    },
    {
      key: "names",
      label: "names",
    },
    {
      key: "container id",
      label: "container id",
    },
    {
      key: "image",
      label: "images",
    },
    {
      key: "command",
      label: "commands",
    },
    {
      key: "created",
      label: "created",
    },
    {
      key: "status",
      label: "status",
    },
    {
      key: "ports",
      label: "ports",
    },
    {
      key: "size",
      label: "size",
    },
    {
      key: "actions",
      label: "actions",
    },
  ];

   useEffect(() => {
    fetchData()
  }, []);

  const fetchData = () => {
    axios.get(`${globalUrl}/api/admin/get-docker-containers`).then((response)=>{
      console.log(response.data);
      console.log(response)
    //   console.log(response.data.image_list[0]);
      setdata(response.data)
    }).catch((error)=>{
    //   console.log(error)
    })
  };

  const handleDelete = (container) =>{

    axios.delete(`${globalUrl}/api/admin/admin-delete-single-container`,{data:{container}}).then((response)=>{
      console.log(response);
      toast.success("Container deleted successfully")
      fetchData()
  
    }).catch((error)=>{
      console.log(error)
      handleAction(container)
    })
    // axios.delete("")
  }

  const handleForceDelete = (container) =>{

    axios.delete(`${globalUrl}/api/admin/admin-force-delete-single-container`,{data:{container}}).then((response)=>{
      console.log(response);
      toast.success("Container deleted successfully")
      fetchData()
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error("Server error")
      document.body.style.cursor = 'default';
    })
    // axios.delete("")
  }

  const handleAction = (container) => {
    const userConfirmed = window.confirm('WARNING!!!!! : The container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      document.body.style.cursor = 'wait';
      handleForceDelete(container)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const handleActionRemove = () => {
    const userConfirmed = window.confirm('WARNING!!!!! : One of the container is currently running. Are you sure you want to delete ?');
    if (userConfirmed) {
      document.body.style.cursor = 'wait';
      handleForceRemove()
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const handleCheckboxChange = (event) => {
    const { value, checked } = event.target;

    if (checked) {
      setCheckedItems([...checkedItems, value]);
    } else {
      setCheckedItems(checkedItems.filter(item => item !== value));
    }
  };

  const handleStart = () => {
    axios.post(`${globalUrl}/api/admin/admin-start-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers started successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleStop = () => {
    document.body.style.cursor = 'wait';
    axios.post(`${globalUrl}/api/admin/admin-stop-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers stopped successfully")
      fetchData()
      setCheckedItems([])
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      document.body.style.cursor = 'default';
      // handleAction(container)
    })
  }

  const handleKill = () => {
    axios.post(`${globalUrl}/api/admin/admin-kill-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers killed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleRestart = () => {
    axios.post(`${globalUrl}/api/admin/admin-restart-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers restarted successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handlePause = () => {
    axios.post(`${globalUrl}/api/admin/admin-pause-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers paused successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleResume = () => {
    axios.post(`${globalUrl}/api/admin/admin-resume-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.success("Containers resumed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      // handleAction(container)
    })
  }

  const handleRemove = () => {
    axios.post(`${globalUrl}/api/admin/admin-remove-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers removed successfully")
      fetchData()
      setCheckedItems([])
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      handleActionRemove()
    })
  }

  const handleForceRemove = () => {
    axios.post(`${globalUrl}/api/admin/admin-force-remove-container`,{data:{checkedItems}}).then((response)=>{
      console.log(response);
      toast.warning("Containers removed successfully")
      fetchData()
      setCheckedItems([])
      document.body.style.cursor = 'default';
    }).catch((error)=>{
      console.log(error)
      toast.error(error.message)
      document.body.style.cursor = 'default';
      // handleAction(container)
    })
  }


  
  return (
    <>
      <main className="h-full">
        <Navbar toggle={sidebarToggle} />

        
      <div className="mainCard">
      <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
        <div className="flex">
          <button className="border py-2 px-3 text-white bg-green-500" onClick={()=>{ handleStart() }}><FontAwesomeIcon icon={faPlay} />Start</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleStop() }}><FontAwesomeIcon icon={faStop} />Stop</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleKill() }}><FontAwesomeIcon icon={faBomb} />Kill</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handleRestart() }}><FontAwesomeIcon icon={faRepeat} />Restart</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handlePause() }}><FontAwesomeIcon icon={faPause} />Pause</button>
          <button className="border py-2 px-3 text-white bg-blue-700" onClick={()=>{ handleResume() }}><FontAwesomeIcon icon={faPlay} />Resume</button>
          <button className="border py-2 px-3 text-white bg-red-700" onClick={()=>{ handleRemove() }}><FontAwesomeIcon icon={faTrashCan} />Remove</button>
        </div>
        </div>
        </div>

        {/* Main Content */}
        <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <ContainersTable
              loading={loading}
              dataHeader={dataHeader}
              data={data}
              handleDelete={handleDelete}
              checkedItems={checkedItems}
              handleCheckboxChange={handleCheckboxChange}
            />
          </div>
        </div>

        <div>
        <div className="pt-2 border-t border-gray-300">
            <div className="py-2 px-4">
              {/* Logout Button */}
              <button
                className="py-2 px-4 border border-emerald-500 bg-emerald-600 w-full rounded-full text-gray-200 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm"
                // onClick={() => logout()}
              >
                <FontAwesomeIcon className="px-2" icon={faPlus}></FontAwesomeIcon> Add 
              </button>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default DockerContainers;
