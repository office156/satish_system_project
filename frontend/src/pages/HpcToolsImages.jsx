import React, { useEffect, useState } from 'react'
import Navbar from "../components/Navbar/Index";
import { useNavigate, useOutletContext } from "react-router-dom";
import axios from 'axios';
import { toast } from 'react-toastify';
import ImagesTable from "./ImagesTable";
import globalUrl from '../data/url';
import DockerContainers from './DockerContainers';
import DockerContainersHpc from './DockerContainersHpc';

export default function HpcToolsImages() {
    const [sidebarToggle] = useOutletContext();
    const [data, setdata] = useState([]);
    const [selectedImage, setselectedImage] = useState("");
    const navigate = useNavigate()
    const [data2 , setdata2] = useState([]);

    const [loading] = useState(false);
  
    const dataHeader = [
      {
        key: "repositary",
        label: "repositary",
      },
      {
        key: "tag",
        label: "tags",
      },
      {
        key: "Image id",
        label: "Image ids",
      },
      {
        key: "Created",
        label: "Created",
      },
      {
        key: "Size",
        label: "Size",
      },
      {
        key: "action",
        label: "actions",
      },
      {
        key: "create",
        label: "create",
      },
    ];
  
    useEffect(() => {
      fetchData()
      fetchData2()
    }, []);
  
    const fetchData = () => {
      axios.get(`${globalUrl}/api/admin/get-docker-cdac-images`).then((response)=>{
        console.log(response.data);
        console.log(response)
      //   console.log(response.data.image_list[0]);
        setdata(response.data)
      }).catch((error)=>{
      //   console.log(error)
      })
    };
  
    const handleDelete = (image , imageName , imageTag) =>{
        document.body.style.cursor = 'wait';
      axios.delete(`${globalUrl}/api/admin/admin-delete-single-image`,{data:{image}}).then((response)=>{
        console.log(response);
        toast.success("Image deleted successfully")
        fetchData()
        document.body.style.cursor = 'default';
      }).catch((error)=>{
        console.log(error)
        const image2 = imageName + ":" + imageTag
        document.body.style.cursor = 'default';
        // handleAction(image,image2)
      })
      // axios.delete("")
    }

    const fetchData2 = () => {
        axios.get(`${globalUrl}/api/admin/get-docker-images-local`).then((response) => {
            console.log(response);

            setdata2(response.data)
        }).catch((error) => {
            //   console.log(error)
        })
    };

    const handleLoad = (containerId) => {
        document.body.style.cursor = 'wait';
        axios.post(`${globalUrl}/api/admin/load-docker-images-local`, { data: { containerId } }).then((response) => {
            console.log(response);

            toast.success("Image loaded successfully")
            fetchData()
            document.body.style.cursor = 'default';
            // navigate("/test234")
        }).catch((error) => {
            //   console.log(error)
            toast.error(error)
            document.body.style.cursor = 'default';
        })
    }

    const array2 = ['cardInfo', 'cardWarning', 'cardDanger', 'cardSuccess', 'cardLime', 'cardDanger']; // Always 5 elements


    const combinedArray = [];
    for (let i = 0; i < data2.length; i++) {
        combinedArray.push([data2[i], array2[i % 5]]);
    }

    return (
        <>
            <main className="h-full">
                <Navbar toggle={sidebarToggle} />


                <div className="mainCard">

                    <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
                        <p className='text-2xl font-bold'>Load Images</p>
                        <br />
                        <br />

                        <div className="grid grid-cols-4 gap-4">

                            {combinedArray.map((item, index) => (
                                <div className={`p-3 rounded ${item[1]} text-slate-50 flex flex-col overflow-x-hidden hover:overflow-x-scroll no-scrollbar`}>
                                    <h1 className="pb-3 font-semibold">{item[0]}</h1>
                                    <span  >
                                        <button className="text-xs px-2 py-1 rounded-full bg-white" onClick={() => {
                                            // setselectedImage(item[0])
                                            handleLoad(item[0])
                                        }}>Load</button>
                                    </span>

                                </div>
                            ))}
                        </div>
                    </div>
                </div>


                <div className="mainCard">

                    <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
                    <p className='text-2xl font-bold'>HPC Tools Images</p>
                        <br />
                        <br />


{/* <div className='flex justify-between'>
    <p>{selectedImage}</p>

    <button className='border ml-4 bg-sky-400 text-white rounded p-2'>Create container</button>
</div> */}


          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <ImagesTable
              loading={loading}
              dataHeader={dataHeader}
              data={data}
              handleDelete={handleDelete}
            />
          </div>

          <a  onClick={() => {
                navigate("/image")
              }} className="anchor_tag font-medium text-sm text-primary text-decoration-underline">
            All images
            </a>

                    </div>


                </div>

                <div className="mainCard">

<div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
    <p className='text-2xl font-bold'>Running Containers</p>
    <br />
    <br />

<DockerContainersHpc></DockerContainersHpc>

<a  onClick={() => {
                navigate("/container")
              }} className="anchor_tag font-medium text-sm text-primary text-decoration-underline">
            All containers
            </a>

</div>



</div>

            </main>
        </>
    )
}
