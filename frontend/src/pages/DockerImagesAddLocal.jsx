import React from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload , faCrosshairs } from '@fortawesome/free-solid-svg-icons';
import "./MyCss.css"

export default function DockerImagesAddLocal() {
    const [sidebarToggle] = useOutletContext();


  return (
    <>
    <main className="h-full">
      <Navbar toggle={sidebarToggle} />



      <div className="mainCard">
          <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
            <div className='text-xl text-slate-500'>
                <span className='rounded-full p-2 mr-2 shadow-md bg-slate-100'>
                <FontAwesomeIcon icon={faDownload} /></span>Pull Image</div>

            <div className="">

            <div className="form mt-5">
                <div className="line1 flex w-full my-4">
                    <span className='mr-2 w-20'>Registry</span>
                    <input type="text" placeholder='Local registry' className='border flex-1 commonInputStyle'/>
                </div>
                <div className="line2 flex">
                    <span className='mr-2 w-20'>Image <span className='text-red-700'>*</span></span>
                    <input type="text" placeholder='e.g.  my image:my tag' className='border flex-1 commonInputStyle'/>
                    <button className='search-button border ml-2'>Search</button>
                </div>
                {/* <div className="line3 mt-3">
                    <a href="/image/add/local" className='text-cyan-600'> <span><FontAwesomeIcon icon={faCrosshairs} /></span> Advanced mode</a>
                </div> */}
                <div className="line4 mt-3">
                    <button className='py-2 px-4 border border-emerald-500 bg-emerald-600 rounded text-gray-200 hover:bg-emerald-600 hover:border-emerald-600 justify-end text-sm'>Pull the image</button>
                </div>
            </div>

            </div>
          </div>
        </div>

      </main>
      </>
  )
}
