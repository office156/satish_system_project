import React from 'react'
import Navbar from "../components/Navbar/Index";
import { useOutletContext } from "react-router-dom";

export default function DockerContainerInspectConsole() {
    const [sidebarToggle] = useOutletContext();

  return (
    <>
      <main className="h-full">
        <Navbar toggle={sidebarToggle} />

        
      <div className="mainCard">
      <div className="border w-full border-gray-200 bg-white py-4 px-6 rounded-md">
        <iframe src="http://10.208.35.160:7681" className='w-full h-screen' frameborder="0" title='dockerconsole'></iframe>
        </div>
        </div>
        </main>
        </>
  )
}
