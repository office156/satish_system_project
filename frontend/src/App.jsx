import React from "react";
import { Route, Routes } from "react-router-dom";

import Dashboard from "./pages/Dashboard";
import Table from "./pages/Table";
import AuthLayout from "./components/Layout/AuthLayout";
import GuestLayout from "./components/Layout/GuestLayout";
import Login from "./pages/auth/Login";
import Blank from "./pages/Blank";
import NotFound from "./pages/NotFound";
import Form from "./pages/Form";
import RegisterIndex from "./pages/auth/Register";
import DockerImages from "./pages/DockerImages";
import DockerContainers from "./pages/DockerContainers";
import DockerImagesAdd from "./pages/DockerImagesAdd";
import DockerImagesLoad from "./pages/DockerImagesLoad";
import DockerImagesSave from "./pages/DockerImagesSave";
import DockerImagesAddLocal from "./pages/DockerImagesAddLocal";
import DockerContainerInfo from "./pages/DockerContainerInfo";
import DockerContainerInspect from "./pages/DockerContainerInspect";
import Testing from "./pages/Testing";
import HpcToolsImages from "./pages/HpcToolsImages";
import DockerImageCreateContainer from "./pages/DockerImageCreateContainer";
import DockerContainerInspectConsole from "./pages/DockerContainerInspectConsole";
import DockerImagesBuild from "./pages/DockerImagesBuild";
import Network from "./pages/Network";
import Testform from "./pages/Testform";
import Volume from "./pages/Volume";
import VolumeForm from "./pages/VolumeForm";
import LandingPage from "./pages/LandingPage";
import LandingPage1 from "./pages/LandingPage1";
import DeployLayout from "./components/Layout/DeployLayout";
import DeployNeworking from "./pages/Deployment/Neworking";
import Passwordless from "./pages/Deployment/Passwordless";
import PcsDrbd from "./pages/Deployment/PcsDrbd";
import HpcStackDeploy from "./pages/Deployment/HpcStackDeploy";
import ManageServices from "./pages/Deployment/ManageServices";
import DeployBenchmarking from "./pages/Deployment/DeployBenchmarking";

function App() {
  return (
    <Routes>
      <Route path="/" element={<LandingPage1></LandingPage1>}></Route>

      <Route path="/deploy" element={<DeployLayout />}>
        <Route path="/deploy/networking" element={<DeployNeworking />}></Route>
        <Route path="/deploy/passwordless" element={<Passwordless />}></Route>
        <Route path="/deploy/pcs-drbd" element={<PcsDrbd />}></Route>
        <Route path="/deploy/hpc-stack" element={<HpcStackDeploy />}></Route>
        <Route path="/deploy/manage-services" element={<ManageServices />}></Route>
        <Route path="/deploy/benchmarking" element={<DeployBenchmarking />}></Route>
      </Route>

      <Route path="/" element={<AuthLayout />}>
        <Route path="/dashboard" element={<Dashboard />}></Route>
        <Route path="/testing" element={<Testing />}></Route>
        <Route path="/test-form" element={<Testform />}></Route>
        <Route path="/table" element={<Table />}></Route>
        <Route path="/image" element={<DockerImages />}></Route>
        <Route path="/network" element={<Network />}></Route>
        <Route path="/add-new-network" element={<Form />}></Route>
        <Route path="/volume" element={<Volume />}></Route>
        <Route path="/add-new-volume" element={<VolumeForm />}></Route>
        <Route path="/hpc-tools" element={<HpcToolsImages/>}></Route>
        <Route path="/container" element={<DockerContainers />}></Route>
        <Route path="/container/info" element={<DockerContainerInfo />}></Route>
        <Route path="/container/inspect" element={<DockerContainerInspect />}></Route>
        <Route path="/container/inspect/console" element={<DockerContainerInspectConsole />}></Route>
        <Route path="/image/add" element={<DockerImagesAdd />}></Route>
        <Route path="/image/build" element={<DockerImagesBuild />}></Route>
        <Route path="/image/container/create" element={<DockerImageCreateContainer />}></Route>
        <Route path="/image/add/local" element={<DockerImagesAddLocal />}></Route>
        <Route path="/image/load" element={<DockerImagesLoad />}></Route>
        <Route path="/image/save" element={<DockerImagesSave />}></Route>
        <Route path="/blank" element={<Blank />}></Route>
        <Route path="/404" element={<NotFound />}></Route>
        <Route path="/form" element={<Form />}></Route>
        <Route path="/profile" element={<Blank />}></Route>
      </Route>
      <Route path="/auth" element={<GuestLayout />}>
        <Route path="/auth/login" element={<Login />}></Route>
        <Route path="/auth/register" element={<RegisterIndex />}></Route>
      </Route>
    </Routes>
  );
}

export default App;
