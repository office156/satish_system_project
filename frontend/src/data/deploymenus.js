import { faPage4, faWindows , faStackOverflow} from "@fortawesome/free-brands-svg-icons";
import {
  faTachometer,
  faTable,
  faLock,
  faNoteSticky,
  faNotdef,
  faCheck,
  faBox,
  faMicrochip,
  faWifi,
  faVial,
  faPanorama,
  faAlignJustify,
  faStroopwafel,
  faLaptop,
  faNetworkWired,
} from "@fortawesome/free-solid-svg-icons";

const deployMenu = [
  {
    label: "Networking",
    path: "/deploy/networking",
    icon: faTachometer,
  },
  {
    label: "Passworless ssh",
    path: "/deploy/passwordless",
    icon: faAlignJustify,
  },
  {
    label: "PCS and DRBD configuration",
    path: "/deploy/pcs-drbd",
    icon: faLaptop,
  },
  {
    label: "HPC software stack",
    path: "/deploy/hpc-stack",
    icon: faStroopwafel,
  },
  {
    label: "Manage services",
    path: "/deploy/manage-services",
    icon: faNetworkWired,
  },
  {
    label: "Benchmarking",
    path: "/deploy/benchmarking",
    icon: faPanorama,
  },
];

export default deployMenu
