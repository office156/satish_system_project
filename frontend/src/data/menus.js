import { faPage4, faWindows , faStackOverflow} from "@fortawesome/free-brands-svg-icons";
import {
  faTachometer,
  faTable,
  faLock,
  faNoteSticky,
  faNotdef,
  faCheck,
  faBox,
  faMicrochip,
  faWifi,
  faVial,
  faPanorama,
  faAlignJustify,
  faStroopwafel,
  faLaptop,
  faNetworkWired,
} from "@fortawesome/free-solid-svg-icons";

const initMenu = [
  {
    label: "Dashboard",
    path: "/",
    icon: faTachometer,
  },
  {
    label: "HPC software stack",
    path: "/hpc-tools",
    icon: faAlignJustify,
  },
  // {
  //   label: "HPC tools",
  //   path: "/hpc-tools",
  //   icon: faAlignJustify,
  // },
  {
    label: "GPU support",
    path: "/gpu-support",
    icon: faLaptop,
  },
  {
    label: "Enroot",
    path: "/enroot",
    icon: faStroopwafel,
  },
  {
    label: "Singularity",
    path: "/singularity",
    icon: faNetworkWired,
  },
  {
    label: 'Menu'
  },
  {
    label: "Image",
    path: "/image",
    icon: faPanorama,
  },
  {
    label: "Container",
    path: "/container",
    icon: faBox,
  },
  {
    label: "Services",
    path: "/services",
    icon: faMicrochip,
  },
  {
    label: "Stack",
    path: "/stack",
    icon: faStackOverflow,
  },
  {
    label: "Network",
    path: "/network",
    icon: faWifi,
  },
  {
    label: "Volume",
    path: "/volume",
    icon: faVial,
  },
  // {
  //   label: "Blank",
  //   path: "/blank",
  //   icon: faPage4,
  // },
  // {
  //   label: "404",
  //   path: "/404",
  //   icon: faNotdef,
  // },
  
  {
    label: 'Tabel dan Form'
  },
  {
    label: "Form",
    path: "/form",
    icon: faWindows,
  },
  {
    label: "Tabel",
    path: "/table",
    icon: faTable,
  },

  {
    label: 'Otentikasi'
  },
  {
    label: "Login",
    path: "/auth/login",
    icon: faLock,
  },
  {
    label: "Register",
    path: "/auth/register",
    icon: faNoteSticky,
  },
];

export default initMenu
